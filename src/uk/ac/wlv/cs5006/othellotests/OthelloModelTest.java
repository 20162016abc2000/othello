package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.Controller;
import uk.ac.wlv.cs5006.othello.Grid;
import uk.ac.wlv.cs5006.othello.OthelloModel;
import uk.ac.wlv.cs5006.othello.Token;

public class OthelloModelTest {

    @Test
    // test the setInitialState() method.
    public void testSetInitialState() {
        OthelloModel model = new OthelloModel(8, 8);
        for (int i = 0; i < model.getNumRows(); i++) {
            for (int j = 0; j < model.getNumColumns(); j++) {
                if (i == 3 && j == 3) {
                    assertTrue(model.getToken(i, j).getValue().equals("BLACK"));
                } else if (i == 4 && j == 4) {
                    assertTrue(model.getToken(i, j).getValue().equals("BLACK"));
                } else if (i == 3 && j == 4) {
                    assertTrue(model.getToken(i, j).getValue().equals("WHITE"));
                } else if (i == 4 && j == 3) {
                    assertTrue(model.getToken(i, j).getValue().equals("WHITE"));
                } else {
                    assertTrue(model.getToken(i, j).getValue().equals("EMPTY"));
                }
            }
        }
    }

    @Test
    // test the get, set methods.
    public void testGetSet() {
        OthelloModel model = new OthelloModel(8, 8);

        assertEquals(8, model.getNumRows());
        assertEquals(8, model.getNumColumns());

        // valid move
        assertTrue(model.attemptPlay(3, 5, new Token("BLACK")));

        // test get token
        assertTrue(model.getToken(3, 5).getValue().equals("BLACK"));

        model.setInitialState();

        for (int i = 0; i < model.getNumRows(); i++) {
            for (int j = 0; j < model.getNumColumns(); j++) {
                if (i == 3 && j == 3) {
                    assertTrue(model.getToken(i, j).getValue().equals("BLACK"));
                } else if (i == 4 && j == 4) {
                    assertTrue(model.getToken(i, j).getValue().equals("BLACK"));
                } else if (i == 3 && j == 4) {
                    assertTrue(model.getToken(i, j).getValue().equals("WHITE"));
                } else if (i == 4 && j == 3) {
                    assertTrue(model.getToken(i, j).getValue().equals("WHITE"));
                } else {
                    assertTrue(model.getToken(i, j).getValue().equals("EMPTY"));
                }
            }
        }
    }

    @Test
    // test the attemptPlay() method with valid and invalid plays. Test parts of
    // the Grid where the player cannot legally play.
    public void testAttemptPlay() {

        OthelloModel model = new OthelloModel(8, 8);

        //null test
        assertFalse(model.attemptPlay(0, 0, null));

        for (int i = 0; i < model.getNumRows(); i++) {
            for (int j = 0; j < model.getNumColumns(); j++) {
                if (i == 3 && j == 5) {
                    // valid move
                    assertTrue(model.attemptPlay(i, j, new Token("BLACK")));
                } else if (i == 5 && j == 3) {
                    // valid move
                    assertTrue(model.attemptPlay(i, j, new Token("BLACK")));
                } else if (i == 4 && j == 2) {
                    // valid move
                    assertTrue(model.attemptPlay(i, j, new Token("BLACK")));
                } else if (i == 2 && j == 4) {
                    // valid move
                    assertTrue(model.attemptPlay(i, j, new Token("BLACK")));
                } else {
                    // invalid move
                    assertFalse(model.attemptPlay(i, j, new Token("BLACK")));
                }
                model = new OthelloModel(8, 8);
            }
        }
    }

    @Test
    // test the gridIsFull() method.
    public void testGridIsFull() {
        OthelloModel model = new OthelloModel(8, 8);

        assertFalse(model.gridIsFull());
        model.getToken(1, 1).setValue("BLACK");
        assertFalse(model.gridIsFull());

        for (int i = 0; i < model.getNumRows(); i++) {
            for (int j = 0; j < model.getNumColumns(); j++) {
                model.getToken(i, j).setValue("WHITE");
            }
        }

        assertTrue(model.gridIsFull());

        model.getToken(0, 1).setValue("BLACK");

        assertTrue(model.gridIsFull());

        model.getToken(1, 1).setValue("EMPTY");
        assertFalse(model.gridIsFull());
    }

    @Test
    // test the gridHasSingleValue() method.
    public void testGridHasSingleValue() {
        // all empty
        OthelloModel model = new OthelloModel(8, 8);
        model.getToken(3, 3).setValue("EMPTY");
        model.getToken(4, 4).setValue("EMPTY");
        model.getToken(3, 4).setValue("EMPTY");
        model.getToken(4, 3).setValue("EMPTY");

        assertTrue(model.gridHasSingleValue());

        // all empty, except one white
        model.getToken(0, 1).setValue("WHITE");
        assertTrue(model.gridHasSingleValue());

        // all empty, except one black
        model.getToken(0, 1).setValue("BLACK");
        assertTrue(model.gridHasSingleValue());

        // all white, one black
        for (int i = 0; i < model.getNumRows(); i++) {
            for (int j = 0; j < model.getNumColumns(); j++) {
                model.getToken(i, j).setValue("WHITE");
            }
        }
        model.getToken(0, 1).setValue("BLACK");
        assertFalse(model.gridHasSingleValue());

        // all black, one white
        for (int i = 0; i < model.getNumRows(); i++) {
            for (int j = 0; j < model.getNumColumns(); j++) {
                model.getToken(i, j).setValue("BLACK");
            }
        }
        model.getToken(0, 1).setValue("WHITE");
        assertFalse(model.gridHasSingleValue());
    }

    @Test
    // test the getOpponent() method with both Token values.
    public void testGetOpponent() {
        OthelloModel model = new OthelloModel(8, 8);

        assertTrue(model.getOpponent("WHITE").equals("BLACK"));
        assertTrue(model.getOpponent("BLACK").equals("WHITE"));
        assertTrue(model.getOpponent("EMPTY").equals("EMPTY"));
    }

    @Test
    // test the getWinner() method.
    public void testGetWinner() {
        OthelloModel model = new OthelloModel(8, 8);
        assertTrue(model.getWinner().equals("NONE"));
        // all black
        for (int i = 0; i < model.getNumRows(); i++) {
            for (int j = 0; j < model.getNumColumns(); j++) {
                model.getToken(i, j).setValue("BLACK");
            }
        }
        assertTrue(model.getWinner().equals("BLACK"));

        // all white
        for (int i = 0; i < model.getNumRows(); i++) {
            for (int j = 0; j < model.getNumColumns(); j++) {
                model.getToken(i, j).setValue("WHITE");
            }
        }
        assertTrue(model.getWinner().equals("WHITE"));

        // one play - draw
        model = new OthelloModel(8, 8);
        assertTrue(model.attemptPlay(3, 5, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(1, 5, new Token("BLACK")));
        assertTrue(model.attemptPlay(3, 6, new Token("WHITE")));
        assertTrue(model.attemptPlay(4, 5, new Token("BLACK")));
        assertTrue(model.attemptPlay(5, 4, new Token("WHITE")));
        assertTrue(model.attemptPlay(3, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(0, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 5, new Token("BLACK")));
        assertTrue(model.attemptPlay(5, 6, new Token("WHITE")));
        assertTrue(model.attemptPlay(4, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(4, 7, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 3, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 7, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(4, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(3, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(3, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(2, 4, new Token("BLACK")));
        assertTrue(model.attemptPlay(5, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(2, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(3, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(0, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(4, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(0, 4, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 3, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(7, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 3, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(1, 4, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 3, new Token("WHITE")));
        assertTrue(model.attemptPlay(1, 3, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 4, new Token("WHITE")));
        assertTrue(model.attemptPlay(4, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(2, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(6, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 0, new Token("BLACK")));
        assertTrue(model.attemptPlay(0, 3, new Token("WHITE")));
        assertTrue(model.attemptPlay(1, 2, new Token("BLACK")));
        assertTrue(model.attemptPlay(0, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(6, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(1, 6, new Token("WHITE")));
        assertTrue(model.attemptPlay(1, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(1, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(0, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(7, 7, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 4, new Token("BLACK")));
        assertTrue(model.attemptPlay(7, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 6, new Token("WHITE")));
        assertTrue(model.attemptPlay(0, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(0, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 0, new Token("BLACK")));
        assertTrue(model.attemptPlay(1, 7, new Token("WHITE")));

        assertTrue(model.getWinner().equals("NONE"));

        // one play - black win
        model = new OthelloModel(8, 8);
        assertTrue(model.attemptPlay(3, 5, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(2, 4, new Token("BLACK")));
        assertTrue(model.attemptPlay(4, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(3, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(2, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(1, 6, new Token("WHITE")));
        assertTrue(model.attemptPlay(3, 2, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 3, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 4, new Token("BLACK")));
        assertTrue(model.attemptPlay(1, 7, new Token("WHITE")));
        assertTrue(model.attemptPlay(1, 2, new Token("BLACK")));
        assertTrue(model.attemptPlay(2, 1, new Token("WHITE")));
        assertTrue(model.attemptPlay(1, 3, new Token("BLACK")));
        assertTrue(model.attemptPlay(0, 4, new Token("WHITE")));
        assertTrue(model.attemptPlay(1, 4, new Token("BLACK")));
        assertTrue(model.attemptPlay(5, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 6, new Token("WHITE")));
        assertTrue(model.attemptPlay(6, 5, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 4, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 3, new Token("BLACK")));
        assertTrue(model.attemptPlay(4, 6, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 3, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 3, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(3, 0, new Token("BLACK")));
        assertTrue(model.attemptPlay(4, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(1, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(6, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(4, 1, new Token("WHITE")));
        assertTrue(model.attemptPlay(5, 2, new Token("BLACK")));
        assertTrue(model.attemptPlay(1, 1, new Token("WHITE")));
        assertTrue(model.attemptPlay(0, 3, new Token("BLACK")));
        assertTrue(model.attemptPlay(0, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(2, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(3, 7, new Token("WHITE")));
        assertTrue(model.attemptPlay(0, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(0, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(0, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(1, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(0, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(7, 7, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 6, new Token("BLACK")));
        assertTrue(model.attemptPlay(7, 5, new Token("WHITE")));
        assertTrue(model.attemptPlay(4, 7, new Token("BLACK")));
        assertTrue(model.attemptPlay(7, 4, new Token("WHITE")));
        assertTrue(model.attemptPlay(2, 0, new Token("BLACK")));
        assertTrue(model.attemptPlay(4, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(3, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(7, 2, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 1, new Token("BLACK")));
        assertTrue(model.attemptPlay(6, 1, new Token("WHITE")));
        assertTrue(model.attemptPlay(6, 0, new Token("BLACK")));
        assertTrue(model.attemptPlay(5, 0, new Token("WHITE")));
        assertTrue(model.attemptPlay(7, 0, new Token("BLACK")));
        assertTrue(model.attemptPlay(0, 0, new Token("WHITE")));

        assertTrue(model.getWinner().equals("BLACK"));
    }
}