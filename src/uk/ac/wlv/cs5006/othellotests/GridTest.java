package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.Grid;

public class GridTest {

    @Test
    // test the Grid constructor. Ensure, using the getters and setters, that
    // the
    // Grid has the dimensions you used and that it was cleared.
    public void testContructor() {
        Grid grid = new Grid(100, 200);
        assertEquals(100, grid.getNumRows());
        assertEquals(200, grid.getNumColumns());

        for (int i = 0; i < grid.getNumRows(); i++) {
            for (int j = 0; j < grid.getNumColumns(); j++) {
                assertTrue(grid.getToken(i, j).getValue().equals("EMPTY"));
            }
        }
    }

    @Test
    // test all of the getters and setters.
    public void testGetSet() {
        Grid grid = new Grid(100, 200);
        assertEquals(100, grid.getNumRows());
        assertEquals(200, grid.getNumColumns());

        assertTrue(grid.setTokenValue(0, 0, "EMPTY"));

        assertFalse(grid.setTokenValue(-1, 100, "WHITE"));
        assertFalse(grid.setTokenValue(100, 100, "WHITE"));
        assertFalse(grid.setTokenValue(0, -1, "WHITE"));
        assertFalse(grid.setTokenValue(0, 200, "WHITE"));
        assertTrue(grid.setTokenValue(0, 199, "WHITE"));
        assertTrue(grid.getToken(0, 199).getValue().equals("WHITE"));

        assertTrue(grid.setTokenValue(99, 199, "BLACK"));
        assertTrue(grid.getToken(99, 199).getValue().equals("BLACK"));

        assertNull(grid.getToken(99, 299));
    }

    @Test
    // test the clear() method.
    public void testClear() {
        Grid grid = new Grid(10, 20);

        assertTrue(grid.setTokenValue(9, 19, "BLACK"));
        grid.clear();

        for (int i = 0; i < grid.getNumRows(); i++) {
            for (int j = 0; j < grid.getNumColumns(); j++) {
                assertTrue(grid.getToken(i, j).getValue().equals("EMPTY"));
            }
        }
    }

    @Test
    // test the isOnGrid() method.
    public void testIsOnGrid() {
        Grid grid = new Grid(10, 20);
        assertFalse(grid.isOnGrid(-1, 10));
        assertFalse(grid.isOnGrid(5, -1));
        assertFalse(grid.isOnGrid(10, 10));
        assertFalse(grid.isOnGrid(5, 20));

        assertTrue(grid.isOnGrid(0, 0));
        assertTrue(grid.isOnGrid(9, 19));
    }

    @Test
    // test the isFull() method.
    public void testGridIsFull() {
        Grid grid = new Grid(10, 20);
        assertFalse(grid.gridIsFull());
        grid.setTokenValue(9, 1, "BLACK");
        assertFalse(grid.gridIsFull());

        for (int i = 0; i < grid.getNumRows(); i++) {
            for (int j = 0; j < grid.getNumColumns(); j++) {
                grid.setTokenValue(i, j, "WHITE");
            }
        }

        assertTrue(grid.gridIsFull());

        grid.setTokenValue(0, 1, "BLACK");

        assertTrue(grid.gridIsFull());

        grid.setTokenValue(9, 1, "EMPTY");
        assertFalse(grid.gridIsFull());
    }

    @Test
    // test the gridHasSingleValue() method.
    public void testGridHasSingleValue() {
        // all empty
        Grid grid = new Grid(10, 20);
        assertTrue(grid.gridHasSingleValue());

        // all empty, except one white
        grid.setTokenValue(0, 1, "WHITE");
        assertTrue(grid.gridHasSingleValue());

        // all empty, except one black
        grid.setTokenValue(0, 1, "BLACK");
        assertTrue(grid.gridHasSingleValue());

        // all white, one black
        for (int i = 0; i < grid.getNumRows(); i++) {
            for (int j = 0; j < grid.getNumColumns(); j++) {
                grid.setTokenValue(i, j, "WHITE");
            }
        }
        grid.setTokenValue(0, 1, "BLACK");
        assertFalse(grid.gridHasSingleValue());

        // all black, one white
        for (int i = 0; i < grid.getNumRows(); i++) {
            for (int j = 0; j < grid.getNumColumns(); j++) {
                grid.setTokenValue(i, j, "BLACK");
            }
        }
        grid.setTokenValue(0, 1, "WHITE");
        assertFalse(grid.gridHasSingleValue());
    }

}
