package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.GridLocation;

public class GridLocationTest {

    @Test
    // test that the default constructor works and that it sets the location to
    // an
    // invalid location.
    public void testDefaultConstructor() {
        GridLocation loc = new GridLocation();
        assertTrue(loc.isInvalid());
    }

    @Test
    // test that the parameterised constructor works with a variety of values.
    public void testParameterisedConstructor() {
        GridLocation loc = new GridLocation(100, 200);
        assertFalse(loc.isInvalid());
        loc = new GridLocation(-1, 200);
        assertTrue(loc.isInvalid());
        loc = new GridLocation(100, -1);
        assertTrue(loc.isInvalid());
        loc = new GridLocation(0, 100);
        assertFalse(loc.isInvalid());
        loc = new GridLocation(100, 0);
        assertFalse(loc.isInvalid());
        loc = new GridLocation(0, 0);
        assertFalse(loc.isInvalid());
    }

    @Test
    // test that setInvalid() sets a location to an invalid value.
    public void testSetInvalid() {
        GridLocation loc = new GridLocation(100, 200);
        loc.setInvalid();
        assertTrue(loc.isInvalid());
    }

    @Test
    // test all of the other getters and setters.
    public void testGetSet() {
        GridLocation loc = new GridLocation(100, 200);
        loc.setRow(100);
        assertEquals(100, loc.getRow());
        loc.setCol(200);
        assertEquals(200, loc.getCol());
        loc.setRow(-1);
        assertEquals(-1, loc.getRow());
        loc.setCol(-1);
        assertEquals(-1, loc.getCol());
    }
}
