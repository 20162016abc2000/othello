package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.GridCell;
import uk.ac.wlv.cs5006.othello.Token;

public class GridCellTest {

    @Test
    // test the constructor with both "WHITE" and "BLACK" and different
    // locations.
    public void testConstructor() {
        GridCell cell = new GridCell(0, 1, new Token("WHITE"));
        assertEquals(0, cell.getLocation().getRow());
        assertEquals(1, cell.getLocation().getCol());
        assertTrue(cell.getToken().getValue().equals("WHITE"));

        cell = new GridCell(100, -1, new Token("BLACK"));
        assertEquals(100, cell.getLocation().getRow());
        assertEquals(-1, cell.getLocation().getCol());
        assertTrue(cell.getToken().getValue().equals("BLACK"));
    }

    @Test
    // test all of the getters and setters.
    public void testGetSet() {
        GridCell cell = new GridCell(0, 1, new Token("WHITE"));
        cell.setValue("BLACK");
        assertEquals(0, cell.getLocation().getRow());
        assertEquals(1, cell.getLocation().getCol());
        assertTrue(cell.getToken().getValue().equals("BLACK"));

        cell.setToken(new Token("WHITE"));
        assertTrue(cell.getToken().getValue().equals("WHITE"));
    }
}
