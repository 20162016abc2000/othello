package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.Token;

public class TokenTest {

    @Test
    // test the constructor with both "WHITE" and "BLACK".
    public void testTokenConstructor() {
        Token token = new Token("WHITE");
        assertTrue(token.getValue().equals("WHITE"));

        token = new Token("BLACK");
        assertTrue(token.getValue().equals("BLACK"));

        token = new Token("EMPTY");
        assertTrue(token.getValue().equals("EMPTY"));
    }

    @Test
    // test that the Token value can be set and 'got' correctly.
    public void testGetSet() {
        Token token = new Token("WHITE");
        assertTrue(token.getValue().equals("WHITE"));

        token.setValue("BLACK");
        assertTrue(token.getValue().equals("BLACK"));
    }

    @Test
    // test that equals() works correctly.
    public void testEqualsToken() {
        Token token1 = new Token("WHITE");
        Token token2 = new Token("WHITE");
        assertTrue(token1.equals(token2));

        Token token3 = new Token("BLACK");
        assertFalse(token1.equals(token3));
    }
}
