package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.Controller;
import uk.ac.wlv.cs5006.othello.OthelloModel;
import uk.ac.wlv.cs5006.othello.Token;

public class ControllerTest {

    @Test
    // test the attemptPlay() method with valid and invalid plays.
    public void testValidInvalidPlay() {
        Controller controller = new Controller(new OthelloModel(8, 8));
        // valid move
        assertTrue(controller.attemptPlay(3, 5, new Token("BLACK")));

        // valid move
        controller = new Controller(new OthelloModel(8, 8));
        assertTrue(controller.attemptPlay(5, 3, new Token("BLACK")));

        // valid move
        controller = new Controller(new OthelloModel(8, 8));
        assertTrue(controller.attemptPlay(4, 2, new Token("BLACK")));

        // valid move
        controller = new Controller(new OthelloModel(8, 8));
        assertTrue(controller.attemptPlay(2, 4, new Token("BLACK")));

        // invalid move
        controller = new Controller(new OthelloModel(8, 8));
        assertFalse(controller.attemptPlay(-1, 4, new Token("BLACK")));
        assertFalse(controller.attemptPlay(2, -1, new Token("BLACK")));
        assertFalse(controller.attemptPlay(2, 4, new Token("WHITE")));
        assertFalse(controller.attemptPlay(2, 4, new Token("EMPTY")));
        assertFalse(controller.attemptPlay(6, 5, new Token("BLACK")));
    }

    @Test
    // test the getOpponent() method with both Token values.
    public void testGetOpponent() {
        Controller controller = new Controller(new OthelloModel(8, 8));
        assertTrue(controller.getOpponent("WHITE").equals("BLACK"));
        assertTrue(controller.getOpponent("BLACK").equals("WHITE"));
        assertTrue(controller.getOpponent("EMPTY").equals("EMPTY"));
    }

    @Test
    // test the getWinner() method with both Token values.
    public void testGetWinner() {
        Controller controller = new Controller(new OthelloModel(8, 8));

        assertTrue(controller.getWinner().equals("NONE"));

        // black > white
        assertTrue(controller.attemptPlay(3, 5, new Token("BLACK")));
        assertTrue(controller.getWinner().equals("BLACK"));

        // white > black
        assertTrue(controller.attemptPlay(2, 5, new Token("WHITE")));
        assertTrue(controller.attemptPlay(4, 2, new Token("BLACK")));
        assertTrue(controller.attemptPlay(4, 5, new Token("WHITE")));
        assertTrue(controller.attemptPlay(2, 4, new Token("BLACK")));
        assertTrue(controller.attemptPlay(4, 1, new Token("WHITE")));
        assertTrue(controller.getWinner().equals("WHITE"));
    }
}
