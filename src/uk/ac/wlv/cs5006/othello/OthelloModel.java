package uk.ac.wlv.cs5006.othello;

/**
 * The OthelloModel class.
 * 
 * @author ianskenny.
 * @since 14/15.
 */

public class OthelloModel extends AbstractModel {
    /**
     * DefaultConstructor. Initialises the game Grid.
     */
    public OthelloModel(int rows, int cols) {
        super(rows, cols);
    }

    /**
     * Clears the Grid and sets the initial configuration.
     */
    public void setInitialState() {
        grid.clear();
        grid.setTokenValue((grid.getNumRows() / 2) - 1, (grid.getNumColumns() / 2) - 1, "BLACK");
        grid.setTokenValue((grid.getNumRows() / 2) - 1, (grid.getNumColumns() / 2), "WHITE");
        grid.setTokenValue((grid.getNumRows() / 2), (grid.getNumColumns() / 2) - 1, "WHITE");
        grid.setTokenValue((grid.getNumRows() / 2), (grid.getNumColumns() / 2), "BLACK");
    }

    /**
     * Gets the token at the position given.
     * 
     * @param row
     *            the row number.
     * @param col
     *            the column number.
     * @return the Token at the position given.
     */
    public Token getToken(int row, int col) {
        return grid.getToken(row, col);
    }

    /**
     * Attempts to play the Token at the location specified. If the move is
     * legal then the play will be made. If the move is not legal it won't be
     * made. Thus if the move isn't legal the Grid will be left in the same
     * state it was in prior to the method call.
     *
     * @param row
     *            the row number.
     * @param col
     *            the column number.
     * @param token
     *            the Token to attempt to play.
     * @return true if the play was legal, false otherwise.
     */
    public boolean attemptPlay(int row, int col, Token token) {
        // {horizontal right, horizontal left, vertical down, vertical up,
        // diagonal right down, diagonal left up, diagonal right up, diagonal
        // left
        // down}
        int[] rowInc = new int[] { 0, 0, 1, -1, 1, -1, 1, -1 };
        int[] colInc = new int[] { 1, -1, 0, 0, 1, -1, -1, 1 };

        boolean successfulPlay = false;

        // cannot play a null Token
        if (token == null) {
            return successfulPlay;
        }
        // is the attempted play location on the Grid?
        if (!grid.isOnGrid(row, col)) {
            return successfulPlay;
        }
        // does the location already have a Token?
        if (!(grid.getToken(row, col).getValue().equals("EMPTY"))) {
            return successfulPlay;
        }
        // save the Token at the play location in case the move is illegal
        String savedVal = grid.getToken(row, col).getValue();

        // play the Token first, then check it
        grid.setTokenValue(row, col, token.getValue());

        for (int i = 0; i < 8; i++) { // fixed: 8 directions, <-- --> \|/  /|\  \  \ / /
            // calculate the starting cell for checking for a legal play
            GridLocation startLocation = new GridLocation(row + rowInc[i], col + colInc[i]);

            // check whether the cells contain a legal move
            CheckResult result = checkCells(startLocation, rowInc[i], colInc[i], token);

            // if there are any tokens to flip (i.e. a legal move) ...
            if (result.numTokensToFlip > 0) {
                // flip the tokens to this player's colour
                flipTokens(result.start, rowInc[i], colInc[i], result.numTokensToFlip, token);
                successfulPlay = true;
            }
        }

        // if we didn't find a successful play, revert the token
        if (!successfulPlay) {
            grid.setTokenValue(row, col, savedVal);
        }
        return successfulPlay;
    }

    /**
     * Flips the given tokens to the opposite colour.
     * 
     * @param start
     *            The location to start flipping from.
     * @param rowInc
     *            The row increment (direction).
     * @param colInc
     *            The column increment (direction).
     * @param numToFlip
     *            The number of tokens to flip.
     * @param token
     *            The desired colour of the Token.
     */
    private void flipTokens(GridLocation start, int rowInc, 
            int colInc, int numToFlip, Token token) {
        int row = start.getRow();
        int col = start.getCol();

        for (int i = 0; i < numToFlip; i++) { // assumes square Grid!        
            grid.setTokenValue(row, col, token.getValue());
            row += rowInc;
            col += colInc;
        }
    }

    /**
     * Check for a legal play in the direction given.
     * 
     * @param start
     *            The location to start checking from.
     * @param rowInc
     *            The row increment (direction).
     * @param colInc
     *            The column increment (direction).
     * @param token
     *            The Token that has been played.
     * @return The CheckResult result.
     */
    private CheckResult checkCells(GridLocation start, int rowInc, 
            int colInc, Token token) {
        CheckResult result = new CheckResult();
        result.start = start;

        boolean done = false;

        int tokenCount = 0;

        int row = start.getRow();
        int col = start.getCol();

        for (/*  */; row < grid.getNumRows() && row >= 0 
                && col < grid.getNumColumns() && col >= 0
                && !done; row += rowInc, col += colInc) { 
            // assumes square Grid!
            Token tok = grid.getToken(row, col);

            if (tok.getValue().equals("EMPTY")) {
                done = true;// change from false to true
            }

            if (tok.getValue().equals(getOpponent(token.getValue()))) {
                // cannot compare by == operator, use equals method
                tokenCount++;
            } else if (tok.getValue().equals(token.getValue())) {
                result.end.setRow(row);
                result.end.setCol(col);
                done = true;
            }
        }

        if (result.end.isInvalid()) {
            result.numTokensToFlip = 0;
        } else {
            result.numTokensToFlip = tokenCount;
        }
        return result;
    }

    /**
     * Checks if the Grid is full.
     * 
     * @return true if the Grid is full, false otherwise (i.e. one or more EMPTY
     *         locations).
     */
    public boolean gridIsFull() {
        return grid.gridIsFull();
    }

    /**
     * Checks if the Grid contains only one colour of Token.
     * 
     * @return true if the Grid has only one colour of Token, false otherwise.
     */
    public boolean gridHasSingleValue() {
        return grid.gridHasSingleValue();
    }

    /**
     * Gets the opponent of the Token.
     * 
     * @param player
     *            the Token to find the opponent of.
     * @return the opponent of the Token passed in.
     */
    public String getOpponent(String player) {
        if (player.equals("BLACK")) {
            return "WHITE";
        }
        if (player.equals("WHITE")) {
            return "BLACK";
        } else {
            return "EMPTY";
        }
    }

    /**
     * Gets the winning Token.
     * 
     * @return the winning Token. EMPTY if a draw.
     */
    public String getWinner() {
        int blackCount = 0;
        int whiteCount = 0;

        for (int i = 0; i < grid.getNumRows(); i++) {
            for (int j = 0; j < grid.getNumColumns(); j++) {
                Token tok = getToken(i, j);
                if (tok.getValue().equals("WHITE")) {
                    whiteCount++;
                } else if (tok.getValue().equals("BLACK")) {
                    blackCount++;
                }
            }
        }

        if (blackCount > whiteCount) {
            return "BLACK";
        }
        if (whiteCount > blackCount) {
            return "WHITE";
        }

        return "NONE"; // draw
    }

    /**
     * The CheckResult class. A private class.
     * 
     * @author ianskenny
     * @since 14/15
     */
    private class CheckResult {
        /**
         * The start location.
         */
        public GridLocation start;

        /**
         * The end location.
         */
        public GridLocation end;

        /**
         * The number of Tokens to flip.
         */
        public int numTokensToFlip;

        /**
         * Constructor.
         */
        public CheckResult() {
            start = new GridLocation();
            end = new GridLocation();

            clear();
        }

        /**
         * Clears the result.
         */
        public void clear() {
            start.setInvalid();
            end.setInvalid();
            numTokensToFlip = 0;
        }
    }
}
