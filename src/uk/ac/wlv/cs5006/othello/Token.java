package uk.ac.wlv.cs5006.othello;

/**
 * The Token class.
 * 
 * @author ianskenny.
 * @since 15/16
 */
public class Token {
    /**
     * The value of this Token.
     */
    protected String value;

    /**
     * Constructor.
     * 
     * @param val
     *            the value to initialise this Token to.
     */
    public Token(String val) {
        value = val;
    }

    /**
     * Sets the value of the Token.
     * 
     * @param val
     *            the value to set the Token to.
     */
    public void setValue(String val) {
        value = val;
    }

    /**
     * Gets the value of this Token.
     * 
     * @return the value of this Token.
     */
    public String getValue() {
        return value;
    }

    /**
     * Tests if another Token has the same value as this Token.
     * 
     * @param other
     *            the Token to test against this Token.
     * @return true if the Tokens have the same value, false otherwise.
     */
    public boolean equals(Token other) {
        return this.value.equals(other.value);
    }
}
