package uk.ac.wlv.cs5006.othello;

/**
 * This class represents a single cell on the Grid. It has a location and a
 * token value.
 * 
 * @author ianskenny
 * @since 14/15
 */
public class GridCell {
    /**
     * The Token at this GridCell.
     */
    private Token token;

    /**
     * The CellLocation of this GridCell.
     */
    private GridLocation location;

    /**
     * Constructor.
     * 
     * @param row
     *            the row number.
     * @param col
     *            the column number.
     * @param token
     *            the Token value.
     */
    public GridCell(int row, int col, Token token) {
        this.token = token;
        location = new GridLocation(row, col);
    }

    /**
     * Sets the Token value.
     * 
     * @param token
     *            the Token value.
     */
    public void setToken(Token token) {
        this.token = token;
    }

    /**
     * Set the value of the Token in this GridCell.
     * 
     * @param val
     *            the value of the Token.
     */
    public void setValue(String val) {
        this.token.setValue(val);
    }

    /**
     * Gets the Token value.
     * 
     * @return the Token at this GridCell.
     */
    public Token getToken() {
        return token;
    }

    /**
     * Get the GridLocation for this GridCell.
     * 
     * @return the GridLocation for this GridCell.
     */
    public GridLocation getLocation() {
        return location;
    }
}
